
#ifndef NEWAGAL_H
#define NEWAGAL_H
#include <QWidget>
#include <QWebView>

class Imagen : public QWidget
{
    Q_OBJECT

private:
    QString path;
    QString imagenSVG;

public:
    Imagen(); //Constructor

    QString getpath (); //Get and put path
    void putpath(QString);

    QString getimagenSVG(); //Get and put imagenSVG
    void putimagenSVG(QString);

public slots:
    void saveSvg();
    void openSvg();

};

#endif // imagen_h
