#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "imagen.h"
#include <QClipboard>
#include <QDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    imagen = new Imagen;

    this->connect(this->ui->pbOpen, &QPushButton::clicked, this, &MainWindow::open);
    this->connect(this->ui->pbSave, &QPushButton::clicked, this, &MainWindow::save);
    //this->connect(this->ui->pbSetText, &QPushButton::clicked, this, &MainWindow::setText);
}

void MainWindow::open()
{
    imagen->openSvg(); // open picture
    QString path1 = this->imagen->getpath();
    QByteArray var1;

    //QString path2 = path1;
    //path1.prepend("file:///");
    //this->ui->webView->setUrl(path1);
    //this->ui->label->setText(path1);

    QFile file(path1);
    if (file.open(QFile::ReadOnly)) {
        var1 = file.readAll();
        this->imagen->putimagenSVG(var1);

        QClipboard *clipboard = QApplication::clipboard();
        //QString originalText = clipboard->text();
        QString newText = this->imagen->getimagenSVG();
        clipboard->setText(newText);

        this->ui->webView->page()->triggerAction(QWebPage::Paste);

        //this->ui->label->setText(newText);
        //this->ui->webView->page()->SelectAll;
        }
    }



void MainWindow::save()
{
    if (this->ui->webView->hasSelection()){

       QString var;
       var = this->ui->webView->selectedText();
       imagen->putimagenSVG(var);

       imagen->saveSvg();
       QString path1 = this->imagen->getpath();

       QString var1;
       QByteArray var2;

       var1 = this->imagen->getimagenSVG();
       var2 = var1.toUtf8();

       QFile file(path1);
       if (file.open(QFile::WriteOnly)) {
        file.write(var2);
        }
    }
    else {
        this->ui->webView->page()->triggerAction(QWebPage::SelectAll);
        /*QString var;
        var = this->ui->webView->selectedText();
        this->ui->webView->page()->triggerAction(QWebPage::Copy);
        this->ui->webView->page()->triggerAction(QWebPage::Paste);
        this->ui->label->setText(var);*/
    }
}

/*void MainWindow::setText()

{
    QString var4 = "Prueba";

    var4 = this->ui->webView->selectedText();

    this->ui->label->setText(var4);

}*/


MainWindow::~MainWindow()
{
    delete imagen;
    delete ui;
}
