#include "imagen.h"
#include <QDialog>
#include <QtSvg>
#include <QPainter>

Imagen::Imagen()
{
    path = "";
    imagenSVG = "";
}

QString Imagen::getpath()
{
  return path;
}

QString Imagen::getimagenSVG()
{
  return imagenSVG;
}

void Imagen::putimagenSVG(QString imagenSVG)
{
  this->imagenSVG = imagenSVG;
}

/*void Imagen::save()
{
    QDialog* dialog = new QDialog();
    dialog->exec();
}*/

void Imagen::saveSvg()
{
    QString newPath = QFileDialog::getSaveFileName(this, tr("Save SVG"),
            path, tr("SVG files (*.svg)"));

    if (newPath.isEmpty())
            return;

    path = newPath;
}

void Imagen::openSvg()
{
    QString newPath = QFileDialog::getOpenFileName(this, tr("Open SVG"),
    path, tr("SVG files (*.svg)"));

    if (newPath.isEmpty())
            return;

    path = newPath;
}

