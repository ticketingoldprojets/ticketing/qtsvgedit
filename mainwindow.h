#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "imagen.h"
#include <QWebView>
#include <QByteArray>
#include <QIODevice>
#include <QFile>
#include <QMetaType>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void save();
    void open();
    //void setText();

    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Imagen* imagen;
};

#endif // MAINWINDOW_H
