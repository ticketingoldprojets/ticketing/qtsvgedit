#-------------------------------------------------
#
# Project created by QtCreator 2015-11-18T14:33:40
#
#-------------------------------------------------

QT       += core gui
QT       += svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
greaterThan(QT_MAJOR_VERSION, 4): QT += webkitwidgets

TARGET = QtSvgEdit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagen.cpp

HEADERS  += mainwindow.h \
    imagen.h

FORMS    += mainwindow.ui
